import "bootstrap/dist/css/bootstrap.min.css";
import FileUpload from "./components/FileUpload";
import NavBar from "./components/NavBar";
import "./App.css";

function App() {
  return (
    <div className="App">
      <NavBar />
      <FileUpload />
    </div>
  );
}

export default App;
