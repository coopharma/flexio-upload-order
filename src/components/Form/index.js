import React from "react";
import { Container, Form, Button, Row, Col } from "react-bootstrap";

class OrderForm extends React.Component {
  constructor() {
    super();

    this.state = {
      address: "",
      city: "",
      fullName: "",
      phone: "",
      email: ""
    };
  }

  render() {
    return (
      <Container fluid>
        <Row>
          <Col md="3"></Col>
          <Col md="6">
            <Form onSubmit={this.submitForm}>
              <Form.Group>
                <Form.Label>Name</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter your name and last name"
                  name="fullName"
                  value={this.state.name}
                  onChange={this.handleChange}
                />
              </Form.Group>
              <Form.Group>
                <Form.Label>Address</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter address"
                  name="address"
                  value={this.state.address}
                  onChange={this.handleChange}
                />
              </Form.Group>
              <Form.Group>
                <Form.Label>City</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter city"
                  name="city"
                  value={this.state.city}
                  onChange={this.handleChange}
                />
              </Form.Group>
              <Form.Group>
                <Form.Label>Phone</Form.Label>
                <Form.Control
                  type="phone"
                  placeholder="Enter phone number"
                  name="phone"
                  value={this.state.phone}
                  onChange={this.handleChange}
                />
              </Form.Group>
              <Form.Group>
                <Form.Label>Email</Form.Label>
                <Form.Control
                  type="email"
                  placeholder="Enter email"
                  name="email"
                  value={this.state.email}
                  onChange={this.handleChange}
                />
              </Form.Group>
              <Button type="submit">My Button</Button>
            </Form>
          </Col>
          <Col md="3"></Col>
        </Row>
      </Container>
    );
  }

  submitForm = (e) => {
    e.preventDefault();
    console.log(this.state);
  };

  handleChange = (e) => {
    const key = e.target.name;
    const value = e.target.value;

    this.setState({
      [key]: value
    });
  };
}

export default OrderForm;
