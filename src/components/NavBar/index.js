import React from "react";
import { Container, Row, Col, Image } from "react-bootstrap";
import logoImg from "../../images/Flexio_color.png";

const NavBar = () => {
  return (
    <Container>
      <Row>
        <Col md="12" className="d-flex justify-content-center">
          <Image src={logoImg} fluid width="200" />
        </Col>
      </Row>
    </Container>
  );
};

export default NavBar;
