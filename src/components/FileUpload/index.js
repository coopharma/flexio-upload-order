import React, { useState, useRef } from "react";
import {
  Container,
  Form,
  Button,
  Row,
  Col,
  Alert,
  Card
} from "react-bootstrap";
import Loader from "../Loader";
import { fileUpload } from "../../fetch/File";

const FileUpload = () => {
  const fileRef = useRef(null);
  const formRef = useRef(null);
  const [files, setFiles] = useState(null);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const [showError, setShowError] = useState(false);
  const [success, setSuccess] = useState(false);
  const [checked, setChecked] = useState(false);
  const [routeNumber, setRouteNumber] = useState("");
  const [driverID, setDriverID] = useState("");

  const postFile = async (e) => {
    e.preventDefault();

    setShowError(false);
    setLoading(true);

    if (!files) {
      setError("Please upload a document.");
      setSuccess(null);
      setLoading(false);
      setShowError(true);
      return;
    }

    if (checked && !routeNumber.length) {
      setError("Please enter a route number.");
      setShowError(true);
      setLoading(false);
      return;
    }

    const formData = new FormData();

    if (!routeNumber && !driverID) {
      formData.append("fileUpload", files, files.name);
    } else {
      formData.append("fileUpload", files, files.name);
      formData.append("driverInfo", routeNumber);
      formData.append("driverInfo", driverID);
    }

    const fileUploadResponse = await fileUpload(formData);

    if (fileUploadResponse.error != undefined) {
      setError(fileUploadResponse.error);
      setShowError(true);
      setLoading(false);
      setSuccess(false);
      return;
    }

    setTimeout(() => {
      setLoading(false);
      setChecked(false);
      setSuccess(fileUploadResponse.message);
    }, 3000);

    // Clear form and state
    formRef.current.reset();
    setFiles(null);
  };

  const onFileChange = (e) => {
    setFiles(e.target.files[0]);
  };

  const clearField = () => {
    // Clear form and state
    // formRef.current.reset();
    fileRef.current.value = null;
    setFiles(null);
    setRouteNumber("");
    setDriverID("");
  };

  const notANumber = (value) => {
    if (isNaN(value)) {
      return false;
    }

    return true;
  };

  return (
    <Container>
      <Row>
        <Col md="3"></Col>
        <Col md="6" className="mt-4">
          <Card>
            <Card.Header>
              <h4>Upload document</h4>
            </Card.Header>
            <Card.Body>
              <Form className="mt-3" onSubmit={postFile} ref={formRef}>
                <Form.Group>
                  <Form.Label>
                    Upload CSV file
                    <small style={{ color: "red" }}> (required)</small>
                  </Form.Label>
                  <Form.Control
                    type="file"
                    size="lg"
                    name="fileUpload"
                    onChange={onFileChange}
                    ref={fileRef}
                    accept=".csv, .txt"
                  ></Form.Control>
                </Form.Group>
                <Form.Group className="d-flex justify-content-end">
                  <Form.Check
                    type="checkbox"
                    label="Add Driver"
                    name="checkDriver"
                    className="m-2"
                    onChange={() => {
                      setChecked(!checked);
                      setRouteNumber("");
                      setDriverID("");
                    }}
                    value={checked}
                    checked={checked}
                  />
                </Form.Group>
                <Form.Group>
                  {checked && (
                    <>
                      <Form.Group className="mb-1">
                        <Form.Label>
                          Route #
                          <small style={{ color: "red" }}> (required)</small>
                        </Form.Label>
                        <Form.Control
                          type="text"
                          name="routeNumber"
                          placeholder="Enter a route number"
                          value={routeNumber}
                          maxLength="15"
                          onChange={(e) => {
                            const textValue = e.target.value;
                            setRouteNumber(textValue);
                          }}
                          // ref={fileRef}
                        ></Form.Control>
                      </Form.Group>
                      <Form.Group className="mt-1">
                        <Form.Label>
                          Driver ID
                          <small style={{ color: "#555" }}> (optional)</small>
                        </Form.Label>
                        <Form.Control
                          type="text"
                          name="DriverID"
                          placeholder="Enter a driver ID"
                          value={driverID}
                          maxLength="15"
                          onChange={(e) => {
                            const textValue = e.target.value;
                            if (!notANumber(textValue)) return;
                            setDriverID(textValue);
                          }}
                          // ref={fileRef}
                        ></Form.Control>
                      </Form.Group>
                    </>
                  )}
                </Form.Group>
                <Form.Group className="mt-2 mb-2 d-inline-block m-2">
                  <Button type="button" variant="primary" onClick={clearField}>
                    Clear
                  </Button>
                </Form.Group>
                <Form.Group className="d-inline-block">
                  <Button type="Submit" variant="primary" disabled={loading}>
                    {loading ? <Loader /> : "Create Order"}
                  </Button>
                </Form.Group>
              </Form>
            </Card.Body>
          </Card>
          {showError && (
            <Alert
              variant="danger"
              className="mt-3"
              onClose={() => setShowError(false)}
              dismissible
            >
              {error}
            </Alert>
          )}
          {success && (
            <Alert variant="success" className="mt-3">
              {success}
            </Alert>
          )}
        </Col>
        <Col md="3"></Col>
      </Row>
    </Container>
  );
};

export default FileUpload;
