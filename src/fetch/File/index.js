const fileUpload = async (formData) => {
  try {
    const fetchResponse = await fetch(
      `${process.env.REACT_APP_FILE_URL}/upload`,
      {
        method: "POST",
        body: formData
      }
    );

    const fetchData = await fetchResponse.json();

    return fetchData;
  } catch (error) {
    console.log("Fetch error fileUpload()", error);
  }
};

export { fileUpload };
